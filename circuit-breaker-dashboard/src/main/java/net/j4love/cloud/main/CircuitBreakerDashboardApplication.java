package net.j4love.cloud.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * @author he peng
 */

@SpringBootApplication
@EnableHystrixDashboard
//@EnableEurekaClient
public class CircuitBreakerDashboardApplication {

    public static void main(String[] args) {
        SpringApplication.run(CircuitBreakerDashboardApplication.class , args);
    }
}
