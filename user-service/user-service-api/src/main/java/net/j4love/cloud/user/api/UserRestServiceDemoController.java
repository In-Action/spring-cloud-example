package net.j4love.cloud.user.api;

import net.j4love.cloud.user.facade.api.UserRestService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * @author he peng
 * @create 2018/1/17 15:07
 * @see
 */

@RestController
public class UserRestServiceDemoController implements UserRestService {


    @GetMapping(
            path = "/time"
    )
    @Override
    public String getUserServiceTime() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS").format(new Date());
    }

//    @Override
//    public String getUserServiceTimeFallback() {
//        return "Sorry! Service Unavailable.Try Again";
//    }
}
