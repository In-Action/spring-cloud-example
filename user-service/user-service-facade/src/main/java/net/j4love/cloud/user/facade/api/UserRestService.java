package net.j4love.cloud.user.facade.api;


import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author he peng
 * @create 2018/1/17 14:57
 * @see
 */

@FeignClient(name = "user-service" , fallbackFactory = UserRestServiceFallbackFactory.class)
public interface UserRestService {

    @GetMapping(
            path = "/time"
    )
//    @HystrixCommand(fallbackMethod = "getUserServiceTimeFallback")
    String getUserServiceTime();

//    String getUserServiceTimeFallback();

}
