package net.j4love.cloud.user.facade.api;

import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author he peng
 */

@Component
public class UserRestServiceFallbackFactory implements FallbackFactory<Object> {

    private static final Logger LOG = LoggerFactory.getLogger(UserRestServiceFallbackFactory.class);

    @Override
    public Object create(Throwable cause) {
        LOG.error("UserRestService Error " , cause);
        return "Sorry! Error";
    }
}
