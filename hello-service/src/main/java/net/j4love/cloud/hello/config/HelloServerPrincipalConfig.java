package net.j4love.cloud.hello.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * @author he peng
 * @create 2018/1/16 13:56
 * @see
 */

@RefreshScope
@Configuration
@ConfigurationProperties(prefix = "principal")
public class HelloServerPrincipalConfig {

    private String name;
    private String email;

    public String getName() {
        return name;
    }

    public HelloServerPrincipalConfig setName(String name) {
        this.name = name;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public HelloServerPrincipalConfig setEmail(String email) {
        this.email = email;
        return this;
    }

    @Override
    public String toString() {
        return "HelloServerPrincipalConfig{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
