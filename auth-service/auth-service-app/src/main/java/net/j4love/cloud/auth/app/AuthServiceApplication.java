package net.j4love.cloud.auth.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

/**
 * @author he peng
 * @create 2018/1/18 17:24
 * @see
 */

@SpringBootApplication
@EnableDiscoveryClient
@EnableAuthorizationServer
@ComponentScan(basePackages = "net.j4love.cloud.auth")
public class AuthServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthServiceApplication.class , args);
    }
}
